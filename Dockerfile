FROM ubuntu:focal
RUN mkdir -p app/lib
ENV DEBIAN_FRONTEND="noninteractive" TZ="Europe/Amsterdam"
RUN apt-get update && apt-get install -y libbit-vector-perl libdata-serializer-perl libmoose-perl libstatistics-basic-perl libyaml-syck-perl libdata-guid-perl cpanminus make dumb-init awscli && apt-get clean
RUN cpanm install MooseX::Test::Role Test2::Bundle::More Test::Mock::Class Time::HiRes::Value
ADD docker/run-and-upload /app
ADD run /app
ADD lib /app/lib/
ENTRYPOINT ["dumb-init", "/app/run-and-upload"]
CMD ["--tagoutput", "default"]
