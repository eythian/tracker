# Tracker

[![test status](https://gitlab.com/eythian/tracker/badges/master/pipeline.svg)](https://gitlab.com/eythian/tracker/pipelines)

A reimplementation of the "Tracker" genetic algorithm path-following "ant" system

Read the [original paper](http://ftp.cs.ucla.edu/tech-report/1990-reports/900047.pdf)[1] for the original specifications. This implements the [DFSA](https://en.wikipedia.org/wiki/DFSA) part of that paper.

> [1] Jefferson, David et al., "Evolution as a theme in artificial life: The genesys/tracker system", University of California (Los Angeles). Computer Science Department, UCLA-AI-90-09, 1990.

The path following ant is describe by a state machine that (by default) can do the following:

* Inspect the square in front of it to see if it contains part of the trail. This determines the next state.
* Turn left, turn right, do nothing, or move forward.
* Jump to the new state, repeat.
* If it moved forward onto a trail square, its score is incremented by one.
* Any trail square that the ant has moved onto is cleared (to prevent moving back and forth over the same spot.)

The default run has the following properties:

* A [default map](lib/Tracker/Map/JohnMuirTrail.pm) that gets progressively more difficult due to squares without trail markings.
* A genotype comprised of 453 bits that encodes a deterministic finite state automata and starting state that can walk the map.
* A mutation rate of 0.1% (i.e. every bit in a new child has that chance of being flipped)
* A recombination rate of 1% (i.e. when two parents are producing one child, as the bit strings are being walked through, there is that chance of the source parent being switched.)
* A population size of 65,536 individuals.
* A maximum of 200 state changes being executed before the running stops for that individual when walking the map.
* A selection process that takes the top 10% of individuals and crossbreeds them completely at random to produce the new population, and applies the mutation operator to that new child.

Right now, changing the run-time parameters (generations to stop after, population size, etc.) is best done by modifying [Tracker.pm](lib/Tracker.pm) and the [run](run) (or [run_sdl](run_sdl)) files.

![Small population run](media/small-pop-run.webm)

## Dependencies

* Bit::Vector
* Data::GUID
* Data::Serializer
* ExtUtils::MakeMaker
* Moose
* MooseX::Test::Role
* Sort::Naturally
* Statistics::Basic
* Test2::Bundle::More
* Test::Mock::Class
* Time::HiRes::Value
* YAML::Syck

For debian/ubuntu:

```
sudo apt install libbit-vector-perl libdata-serializer-perl libmoose-perl libstatistics-basic-perl libyaml-syck-perl libgraphics-gnuplotif-perl libdata-guid-perl libsort-naturally-perl cpanminus
cpanm install --sudo MooseX::Test::Role Test2::Bundle::More Test::Mock::Class Time::HiRes::Value
```

Optional, for graphics output:

```
sudo apt install libsdl-perl
```

## Running

```
perl Makefile.PL
make test
./run
OR
./run --sdl
```

### Run Options

* `--sdl`

  Enable rendering an animation of the best result every generation.
* `--generations`

  The number of generations to run for. Default 5000.
* `--bestscore`

  The score that will terminate the run. Default is that supplied by the map used, if possible. If that's not possible there is no maximum score and the total number of generations will be run.

* `--populationsize`

  The population size. Default 65,536 (to match the paper.)

* `--tagoutput`

  If provided, the output filenames have this at the start of them. This can be used to group the output of a set of runs with identical parameters together for `plotruns`.

By default, some basic run summary data is output into `runresults`.

### `plotruns` Options

`plotruns` is a simple tool that reads in the output data and outputs a gnuplot script to display it.

It is best used with data files made using `--tagoutput`, it can use this to group them appropriately.

* `--tagoutput`

  If the output tag can't be worked out from the filenames, you can provide it. It is assumed that this applies to all files (i.e. you can't do useful grouping with this.)

* `--outputdir`

  Override the default output directory. By default the output directory is the directory with all the source files (if they're all in one place.)

* `--combinemode`

  How to merge sets of results together. Sets are grouped by their tag. Default is `none`.

  * `none`

    Display everything individualy.

  * `median`

    Calculate and chart the median of the group of results.

## Docker stuff

There's a docker file that will run the application. The default entrypoint is a script that will run tracker with the arguments provided and upload the result to an S3 bucket. It's designed for doing a lot of runs at once using AWS Batch.
