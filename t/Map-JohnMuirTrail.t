use strict;
use warnings;

use Test2::Bundle::More;

use Tracker::Map::JohnMuirTrail;

plan 27;

my $map_obj = Tracker::Map::JohnMuirTrail->new();

my $map = $map_obj->{_map};

is(@$map, 32, "Map has 32 columns");
is($map->[0]->@*, 32, "Map has 32 rows");
# Check a few points
ok( !$map->[0][0],   "Map location 0,0" );
ok( $map->[31][5],   "Map location 31,5" );
ok( $map->[18][25],  "Map location 18,25" );
ok( !$map->[25][18], "Map location 25,18" );
ok( $map->[0][5],    "Map location 0,5" );
ok( !$map->[21][12], "Map location 21,12" );

my $map_clone = $map_obj->get_map_clone();
is( ref($map_clone), 'ARRAY', "Clone is an arrayref");
is( ref($map_clone->[0]), 'ARRAY', "Clone first element is an arrayref");
is( $map_clone->[0]->@*, 32, "Clone first element has 32 rows");
ok( !$map_clone->[0][0],   "Clone map location 0,0" );
ok( $map_clone->[31][5],   "Clone map location 31,5" );
ok( $map_clone->[18][25],  "Clone map location 18,25" );
ok( !$map_clone->[25][18], "Clone map location 25,18" );
ok( $map_clone->[0][5],    "Clone map location 0,5" );
ok( !$map_clone->[21][12], "Clone map location 21,12" );

my $human_map = $map_obj->get_human_map();
is( ref($human_map),        'ARRAY', "Human is an arrayref" );
is( ref( $human_map->[0] ), 'ARRAY', "Human first element is an arrayref" );
is( $human_map->[0]->@*,    32,      "Human first element has 32 rows" );
is( $human_map->[0][0],     0,       "Human map location 0,0" );
is( $human_map->[31][5],    1,       "Human map location 31,5" );
is( $human_map->[18][25],   1,       "Human map location 18,25" );
is( $human_map->[25][18],   0,       "Human map location 25,18" );
is( $human_map->[0][5],     1,       "Human map location 0,5" );
is( $human_map->[21][12],   0,       "Human map location 21,12" );
is( $human_map->[14][4],    2,       "Human map location 14,4" );
