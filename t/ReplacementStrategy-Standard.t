use strict;
use warnings;

use Test2::Bundle::More;

use MooseX::Test::Role;
use Test::Mock::Class qw( :all );
use Tracker::ReplacementStrategy::Standard;

plan 3;

my @selected;
my $cross_count=0;
my $genotype = consuming_object(
    'Tracker::Genotype',
    methods => {
        crossbreed => sub {
            push @selected, $_[1], $_[2];
            return 'crossbread-' . $cross_count++;
          },
      }
);

my $strategy = Tracker::ReplacementStrategy::Standard->new(
    percent_to_take => 50,
    genotype => $genotype,
);

my @initial_pop = (
    { score => 1, rep => 'rep-1' },
    { score => 2, rep => 'rep-2' },
    { score => 3, rep => 'rep-3' },
    { score => 4, rep => 'rep-4' },
    { score => 5, rep => 'rep-5' },
    { score => 6, rep => 'rep-6' },
);

my $new_pop = $strategy->make_new_generation(\@initial_pop);

is(@$new_pop, 6, "New population is the right size");

# We don't know what got chosen for crossbreeding, but we know that it
# shouldn't be anything with a score 3 or under. This is random though, so
# may sometimes pass when it shouldn't.
is(scalar @selected, 12, "Right number went into crossbreeding");
foreach my $r (@selected) {
    my ($score) = ($r =~ /rep-(\d+)/);
    fail("Attempted to crossbreed something with a score less than 4") if $score <= 3;
}

# All the representations should be 'crossbread'
my %reps;
foreach my $r (@$new_pop) {
    $reps{$r->{rep}} = 1;
    fail("Got a non-'crossbread' representation.") if $r->{rep} !~ /crossbread/;
}

# We should have 6 unique things
is(scalar keys %reps, 6, "Six unique representations");
