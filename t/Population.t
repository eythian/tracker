use strict;
use warnings;

use Test2::Bundle::More;

use MooseX::Test::Role;
use Test::Mock::Class qw( :all );
use Tracker::Evaluator;
use Tracker::Population;

plan 4;

my $genotype = consuming_object('Tracker::Genotype',
    methods => {
        random => sub { 'random-rep' },
    }
);

my $score = 1;
my $evaluator = consuming_object('Tracker::Evaluator',
    methods => {
        get_score => sub { ++$score },
    }
);

my $strategy = consuming_object(
    'Tracker::ReplacementStrategy',
    methods => {

        # just give the same thing back without scores
        make_new_generation => sub {
            [ map { {rep => $_->{rep}} } $_[1]->@* ]
        },
    }
);

my $statistics = mock_class('Tracker::Statistics')->new_object();
my ($generation, $collected);
$statistics->mock_return('add_population',
    sub {
        $generation = $_[2];
        $collected = $_[3];
    }
);

my $population = Tracker::Population->new(
    genotype => $genotype,
    evaluator => $evaluator,
    size => 5,
    replacement_strategy => $strategy,
    statistics => $statistics,
);

$population->step;

my @expected = (
    { score => 2, rep => 'random-rep', },
    { score => 3, rep => 'random-rep', },
    { score => 4, rep => 'random-rep', },
    { score => 5, rep => 'random-rep', },
    { score => 6, rep => 'random-rep', },
);
is($generation, 1, "Provided generation looks correct");
is_deeply($collected, \@expected, "Provided statistics look correct");

$population->step;

@expected = (
    { score => 7, rep => 'random-rep', },
    { score => 8, rep => 'random-rep', },
    { score => 9, rep => 'random-rep', },
    { score => 10, rep => 'random-rep', },
    { score => 11, rep => 'random-rep', },
);
is($generation, 2, "Provided generation looks correct (gen 2)");
is_deeply($collected, \@expected, "Provided statistics look correct (gen 2)");


