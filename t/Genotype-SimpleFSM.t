use strict;
use warnings;

use Test2::Bundle::More;

use Tracker::Genotype::SimpleFSM;

plan 21;

use constant NUM_STATES => 32;

my $geno = Tracker::Genotype::SimpleFSM->new( mutation_rate => 1 );

my $rep = $geno->random();

# should be a list containing 1 + NUM_STATES × 4 values.
is( scalar(@$rep), 1 + NUM_STATES * 4, 'Correct state machine size' );

# test converting a representation to a state machine
my @rep      = (qw(0 L 0 M 1 L 1 M 3 R 2 M 3 R 2 M 2));
my @expected = (
    [ [qw( L 0 )], [qw( M 1 )] ],
    [ [qw( L 1 )], [qw( M 3 )] ],
    [ [qw( R 2 )], [qw( M 3 )] ],
    [ [qw( R 2 )], [qw( M 2 )] ],
);
my @dfsa = $geno->get_state_table( \@rep );
is( $dfsa[0], 0, "Start state is zero" );
is_deeply( $dfsa[1], \@expected, "Generated state table matches" );

# 100% mutation means we need to make sure all the values differ
my @mutant = @rep;
$geno->mutate( \@mutant );
for my $i ( 0 .. $#rep ) {
    isnt( $mutant[$i], $rep[$i], "Mutation changed value $i" );
}
note('Mutated representation is: (' . join(',',@mutant) . ')');
#                   0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6
my @rep1     = (qw( 0 L 0 L 0 L 0 L 0 L 0 L 0 L 0 L 0 ));
my @rep2     = (qw( 1 R 1 R 1 R 1 R 1 R 1 R 1 R 1 R 1 ));

@expected = (qw( 1 R 1 R 0 L 0 R 1 R 1 R 0 L 0 R 1 ));
my $child = $geno->crossbreed(\@rep1, \@rep2, 1, 4, 7, 12, 15);
is_deeply($child, \@expected, "Crossover happened right");
