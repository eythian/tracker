use strict;
use warnings;

use Test2::Bundle::More;

use Tracker::Statistics;

plan 3;

my $stats = Tracker::Statistics->new();
$stats->begin();

my @test_pop = (
    { score => 1, rep => 'rep-1' },
    { score => 2, rep => 'rep-2' },
    { score => 3, rep => 'rep-3' },
    { score => 4, rep => 'rep-4' },
    { score => 5, rep => 'rep-5' },
    { score => 6, rep => 'rep-6' },
    { score => 6, rep => 'rep-7' },
);

$stats->add_population( 1, \@test_pop );

my $summary  = $stats->get_latest_summary();
my %expected = (
    best        => 6,
    worst       => 1,
    mean        => 3.86,
    median      => 4,
    stddev      => 1.81,
    max_mode    => 6,
    percentiles => [ 6, 6, 6, 5, 4, 2 ],
    time        => $summary->{time},
    timestamp   => $summary->{timestamp},
);

is_deeply( $summary, \%expected, "Stats look right" );

my $gen = $stats->get_latest_generation();

is( $gen, 1, "Correct generation number" );

# Mode is a bit weird, so test it specially
@test_pop = (
    { score => 1, rep => 'rep-1' },
    { score => 2, rep => 'rep-2' },
    { score => 3, rep => 'rep-3' },
    { score => 5, rep => 'rep-4' },
    { score => 5, rep => 'rep-5' },
    { score => 6, rep => 'rep-6' },
    { score => 6, rep => 'rep-7' },
);

$stats->add_population( 2, \@test_pop );
$summary = $stats->get_latest_summary();
is( $summary->{max_mode}, 6, "Multimode works right" );
