# -*- perl -*-

# t/001_load.t - check module loading and create testing directory

use Test::More tests => 2;

BEGIN { use_ok( 'Tracker' ); }

my $object = Tracker->new( end_decider => sub { }, population_size => 42 );
isa_ok ($object, 'Tracker');


