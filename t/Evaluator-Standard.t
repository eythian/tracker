use strict;
use warnings;

use Test2::Bundle::More;

use MooseX::Test::Role;
use Test::Mock::Class qw( :all );
use Tracker::DFSA;
use Tracker::Evaluator::Standard;

plan 1;

my $map = consuming_object('Tracker::Map');
my $dfsa = mock_class('Tracker::DFSA')->new_object(map => $map);

my $genotype = consuming_object('Tracker::Genotype',
    methods => {
        get_state_table => sub { ( $_[1] * 2 ) },
    }
);
$dfsa->mock_return( 'run',
    sub {
        { trail_squares => $_[2] };
    }
);

my $evaluator = Tracker::Evaluator::Standard->new(dfsa => $dfsa, genotype => $genotype);

my $rep = '8';
my $score = $evaluator->get_score($rep);
is($score, 16, "Score returned correctly");
