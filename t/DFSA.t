use strict;
use warnings;

use Test2::Bundle::More;

use Tracker::DFSA;
use Tracker::Map::JohnMuirTrail;

plan 3;

my $map_obj = Tracker::Map::JohnMuirTrail->new();
my $dfsa = Tracker::DFSA->new(map => $map_obj);

# state table that just loops, score 0
my @table = (
    [ [ qw( N 0 ) ], [ qw ( N 0 ) ] ],
);
is($dfsa->run(0, \@table)->{trail_squares}, 0, "Trivial loop");

# state table for half-decent algorithm from the paper
@table = (
    [ [ qw( L 0 ) ], [ qw( M 1 ) ] ],
    [ [ qw( L 1 ) ], [ qw( M 3 ) ] ],
    [ [ qw( R 2 ) ], [ qw( M 3 ) ] ],
    [ [ qw( R 2 ) ], [ qw( M 2 ) ] ],
);
is($dfsa->run(0, \@table)->{trail_squares}, 42, 'Clumsy manual solution');

# state table for a solver that gets 81 in 20 generations
@table = (
    [ [ qw( R 1 ) ], [ qw( M 0 ) ] ],
    [ [ qw( R 2 ) ], [ qw( M 0 ) ] ],
    [ [ qw( R 3 ) ], [ qw( M 0 ) ] ],
    [ [ qw( R 4 ) ], [ qw( M 0 ) ] ],
    [ [ qw( M 0 ) ], [ qw( M 0 ) ] ],
);
is($dfsa->run(0, \@table)->{trail_squares}, 81, 'Five state manual solution');


