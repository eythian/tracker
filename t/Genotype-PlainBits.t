use strict;
use warnings;

use Test2::Bundle::More;

use Tracker::Genotype::PlainBits;

use Bit::Vector;

use constant REP_LENGTH => 453;

plan 15;

# We use a 100% mutation rate for testing.
my $geno = Tracker::Genotype::PlainBits->new( mutation_rate => 1 );

# Looking in to the representation a bit, it should produce a random bit
# vector REP_LENGTH bits long.

my $rep = $geno->random();
is( $rep->Size(), REP_LENGTH, "Bit::Vector is REP_LENGTH bytes long" );

# It's extremely unlikely that the value is all 1 or all 0
my @set_bits = $rep->Interval_Scan_inc(0);
ok( scalar @set_bits,                              "At least some 1 bits" );
ok( !( $set_bits[0] == 0 && $set_bits[1] == REP_LENGTH-1 ), "Not everything a 1 bit" )
  if @set_bits;

my $backup_rep = $rep->Clone();
$geno->mutate($rep);
ok( $rep->Compare($backup_rep), "Mutation changed something" );

# because mutation rate is 100%, anding the two values should return all 0.
my $and = Bit::Vector->new(REP_LENGTH);
$and->And( $rep, $backup_rep );
my @and_bits_set = $and->Interval_Scan_inc(0);
is_deeply( \@and_bits_set, [],
    "100% mutation leads to inverted representation" );

# state table creation

# Verify non-zero starting state works
my $state_start_bin = '11001'; # Reads MSB first
$rep = Bit::Vector->new(REP_LENGTH);
$rep->from_Bin($state_start_bin);
my @dfsa = $geno->get_state_table($rep);
is($dfsa[0], 25, "Start state is correct");

# The binary numbers are in little endian because we reverse things
my @state_table_human = qw(00000
00000 01 10000 11
10000 01 11000 11
01000 10 11000 11
01000 10 01000 11 );
# reverse because of how Bit::Vector does things
my $state_table_bin = reverse join('', @state_table_human);
$rep = Bit::Vector->new(REP_LENGTH);
$rep->from_Bin($state_table_bin);
@dfsa = $geno->get_state_table($rep);
is($dfsa[0], 0, "Start state is zero");

# This isn't all 32 states because unreachable ones are optimised out
my @expected = (
    [ [ qw( L 0 ) ], [ qw( M 1 ) ] ],
    [ [ qw( L 1 ) ], [ qw( M 3 ) ] ],
    [ [ qw( R 2 ) ], [ qw( M 3 ) ] ],
    [ [ qw( R 2 ) ], [ qw( M 2 ) ] ],
);
is_deeply($dfsa[1], \@expected, "Generated state table matches");

# Test crossover
my $rep1 = Bit::Vector->new(REP_LENGTH);
my $rep2 = Bit::Vector->new(REP_LENGTH);

$rep1->Empty();
$rep2->Fill();

my $child = $geno->crossbreed($rep1, $rep2, 0, 42, 123);
ok(!$child->bit_test(0), "Bit 0 cleared");
ok(!$child->bit_test(41), "Bit 41 cleared");
ok($child->bit_test(42), "Bit 42 set");
ok($child->bit_test(100), "Bit 100 set");
ok($child->bit_test(122), "Bit 122 set");
ok(!$child->bit_test(123), "Bit 123 cleared");
ok(!$child->bit_test(200), "Bit 200 cleared");

