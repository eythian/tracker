package Tracker;

# This runs the tracker system. Classes can be provided on init to change
# behaviour, otherwise defaults are used.
#
# It will run until the end condition, as defined by the provided sub which
# is passed a reference to the statistics object.

use Moose;
use namespace::autoclean;

use v5.10;

use Tracker::DFSA;
use Tracker::Evaluator::Standard;
use Tracker::Genotype;
use Tracker::Genotype::PlainBits;
use Tracker::Map::JohnMuirTrail;
use Tracker::Population;
use Tracker::Recorder;
use Tracker::ReplacementStrategy::Standard;
use Tracker::Renderer;
use Tracker::Statistics;

# Some defaults
use constant {
    PERCENT_TO_TAKE    => 10,
    MUTATION_RATE      => 0.001,
    RECOMBINATION_RATE => 0.01,
};

has 'end_decider' => (
    isa      => 'CodeRef',
    is       => 'ro',
    required => 1,
);

has 'population_size' => (
    isa      => 'Int',
    is       => 'ro',
    required => 1,
);

has 'tag_output' => (
    isa => 'Str',
    is  => 'ro',
);

has 'dfsa' => (
    isa     => 'Tracker::DFSA',
    is      => 'ro',
    default => sub { Tracker::DFSA->new( map => $_[0]->map ) },
    lazy    => 1,
);

has 'map' => (
    does    => 'Tracker::Map',
    is      => 'ro',
    default => sub { Tracker::Map::JohnMuirTrail->new() },
    lazy    => 1,
);

has 'genotype' => (
    does    => 'Tracker::Genotype',
    is      => 'ro',
    default => sub {
        Tracker::Genotype::PlainBits->new(
            mutation_rate      => MUTATION_RATE,
            recombination_rate => RECOMBINATION_RATE,
        );
    },
    lazy => 1,
);

has 'population' => (
    isa     => 'Tracker::Population',
    is      => 'ro',
    default => sub {
        Tracker::Population->new(
            genotype             => $_[0]->genotype,
            evaluator            => $_[0]->evaluator,
            replacement_strategy => $_[0]->replacement_strategy,
            statistics           => $_[0]->statistics,
            size                 => $_[0]->population_size,
        );
    },
    lazy => 1,
);

has 'replacement_strategy' => (
    does    => 'Tracker::ReplacementStrategy',
    is      => 'ro',
    default => sub {
        Tracker::ReplacementStrategy::Standard->new(
            genotype        => $_[0]->genotype,
            percent_to_take => PERCENT_TO_TAKE,
        );
    },
    lazy => 1,
);

has 'evaluator' => (
    does    => 'Tracker::Evaluator',
    is      => 'ro',
    default => sub {
        Tracker::Evaluator::Standard->new(
            dfsa     => $_[0]->dfsa,
            genotype => $_[0]->genotype
        );
    },
    lazy => 1,
);

has 'statistics' => (
    isa     => 'Tracker::Statistics',
    is      => 'ro',
    default => sub {
        Tracker::Statistics->new(
            recorder => $_[0]->recorder,
            renderer => $_[0]->renderer,
        );
    },
    lazy => 1,
);

has 'renderer' => (
    does    => 'Maybe[Tracker::Renderer]',
    is      => 'ro',
    default => sub {
        my $class = $_[0]->renderer_class;
        if ($class) {
            eval "require $class" or die "Failed to load $class: $@";
            return $class->new(
                map               => $_[0]->map,
                genotype          => $_[0]->genotype,
                dfsa              => $_[0]->dfsa,
                shutdown_callback => do {
                    my ($self) = @_;
                    sub {
                        $self->shutdown();
                    }
                },
            );
        }
        return undef;
    },
    lazy => 1,
);

has 'renderer_class' => (
    isa => 'Str',
    is  => 'ro',
);

has 'recorder' => (
    isa     => 'Tracker::Recorder',
    is      => 'ro',
    default => sub {
        Tracker::Recorder->new(
            genotype => $_[0]->genotype,
            ( tag_output => $_[0]->tag_output ) x !!$_[0]->tag_output,
        );
    },
    lazy => 1,
);

sub run {
    my ($self) = @_;

    my $params = {
        population_size             => $self->population_size,
        mutation_rate               => MUTATION_RATE,
        recombination_rate          => RECOMBINATION_RATE,
        genotype_class              => ref $self->genotype,
        map_class                   => ref $self->map,
        evaluator_class             => ref $self->evaluator,
        replacement_strategry_class => ref $self->replacement_strategy,
        population_class            => ref $self->population,
    };
    print $self->statistics->begin($params);
    my $max_score = $self->map->get_max_score();
    while (1) {
        $self->population->step();
        print $self->statistics->get_latest_text_summary();
        if (
               $self->end_decider->( $self->statistics )
            || $self->{_shutdown}
            || ( defined $max_score
                && $self->statistics->get_latest_summary->{best} >= $max_score )
          )
        {
            print $self->statistics->end();
            say "End condition reached.";
            exit;
        }
    }

}

sub shutdown {
    my ($self) = @_;

    $self->{_shutdown} = 1;
}

__PACKAGE__->meta->make_immutable;
