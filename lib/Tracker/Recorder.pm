package Tracker::Recorder;

# This gets serialised forms of a genotype representation (probably the best)
# and saves them to disk.

use Moose;
use namespace::autoclean;

use Data::GUID;
use Data::Serializer;
use Data::Serializer::Raw;
use File::Path qw( make_path );
use POSIX;
use Sys::Hostname qw(hostname);

use Tracker::Genotype;

# Where to save things to. Files will be in date-named directories under this.
has 'path' => (
    isa     => 'Str',
    is      => 'ro',
    default => './runresults',
);

# If set, output filenames are prefixed with this
has 'tag_output' => (
    isa => 'Str',
    is => 'ro',
);

has 'serialiser' => (
    is      => 'ro',
    default => sub {
        Data::Serializer::Raw->new( serializer => 'YAML::Syck', );
    },
);

has 'genotype' => (
    does     => 'Tracker::Genotype',
    is       => 'ro',
    required => 0,
);

# Describe inital parameters, these will be serialised along with everything
# else.
sub init {
    my ($self, $params) = @_;

    unless ( $self->genotype ) {
        die "'genotype' is required to be provided for output from "
          . ref($self) . "\n";
    }
    $self->{_accumulate}{parameters} = $params;

    my $file_prefix = $self->tag_output ? $self->tag_output . '_' : '';
    my $hostname = hostname();

    # Helps with uniqueness if doing many parallel runs
    my $guid = Data::GUID->new()->as_string();
    $self->{_run_file} //=
        $self->path
      . "/$file_prefix"
      . strftime( "%FT%T", localtime time )
      . "_$guid.dump";
    make_path( $self->path );
}

# Save the representations in the arrayref C<$to_save> as part of generation
# C<$gen>. $to_save should contain a hashref with the rep (which will be
# serialised through the genotype object) and any interesting metadata, like
# score.
sub save {
    my ($self, $gen, $stats, $genos) = @_;

    my @out;
    foreach my $r (@$genos) {
        push @out, {
            %$r,
            rep => $self->genotype->serialise($r->{rep}),
        };
    }
    $self->{_accumulate}{generations}[$gen] =
      { stats => $stats, genotypes => \@out };

    # Write to disk every 100 generations
    if ($gen % 100 == 0) {
        $self->_write_to_disk();
    }
}

sub end {
    my ($self) = @_;

    $self->_write_to_disk();
}

sub _write_to_disk {
    my ($self) = @_;

    my $file = $self->{_run_file};
    open(my $fh, '>', $file) or die "Unable to open file $file: $!\n";

    $self->serialiser->store($self->{_accumulate}, $fh);
    close $fh;
}

# Loads a file from the disk and reverses the serialisation done to it,
# returning a useful record.
#
# Options is a hashref:
# * no_genotype: remove the actual genotype (this can make for faster loading)
sub load {
    my ( $self, $file, $options ) = @_;

    my $fh;
    if ( $file =~ /\.bz2$/ ) {
        -r $file or die "Can't read $file\n";
        open( $fh, "bzcat $file |" ) or die "Couldn't fork: $!";
    }
    else {
        open( $fh, $file ) or die "Couldn't open $file: $!";
    }
    my $record         = $self->serialiser->retrieve($fh);
    my $genotype_obj;
    unless ( $options->{no_genotype} ) {
        my $genotype_class = $record->{parameters}{genotype_class};
        eval "require $genotype_class"
          or die "Unable to load $genotype_class: $@\n";
        $genotype_obj = $genotype_class->new();
    }
    close $fh;
    for my $generation ( $record->{generations}->@* ) {
        next
          unless $generation
          && exists $generation->{genotypes}
          && ref $generation->{genotypes} eq 'ARRAY';
        my $genotypes = $generation->{genotypes};

        for my $geno (@$genotypes) {
            if ($genotype_obj) {
                $geno->{rep} = $genotype_obj->deserialise( $geno->{rep} );
            }
            else {
                $geno->{rep} = undef;
            }
        }
    }
    return $record;
}

__PACKAGE__->meta->make_immutable;
