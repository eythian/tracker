package Tracker::Genotype;

use Moose::Role;

# This role specifies the functions for managing a genotype representation.

# Take a genotype representation and turn it into a state table for the DFSA.
# Should return a tuple where the first entry is the initial state, and the
# second is the nested array described in L<Tracker::DFSA>.
requires 'get_state_table';

# Create a new, random genotype
requires 'random';

# Mutate the representation, according to probabilities and details
# specific to the implementation. i.e. this will be called a lot and
# may do nothing.
requires 'mutate';

# Combine two representations and produce two new ones.
requires 'crossbreed';

# Serialise the representation if needed for easier/nicer output.
requires 'serialise';

# Deserialise the above
requires 'deserialise';

1;
