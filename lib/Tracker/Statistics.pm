package Tracker::Statistics;

# Collects statistics for later analysis

use Moose;
use namespace::autoclean;

use List::Util qw( sum );
use Statistics::Basic qw( mean median mode stddev );
use Time::HiRes::Value;
use Tracker::Recorder;

has 'recorder' => (
    isa      => 'Tracker::Recorder',
    is       => 'ro',
    required => 0,
);

has 'renderer' => (
    does => 'Maybe[Tracker::Renderer]',
    is   => 'ro',
);

# To be called before anything happens in case an initial state needs to
# be set up.
sub begin {
    my ($self, $params) = @_;

    if ($self->renderer) {
        $self->renderer->init();
    }

    if ($self->recorder) {
        $self->recorder->init($params);
    }

    $self->{_start_time} = Time::HiRes::Value->now();
    return;
}

# Allow a summary line or whatever to be written.
sub end {
    my ($self) = @_;

    if ( $self->recorder ) {
        $self->recorder->end();;
    }

    my $now = Time::HiRes::Value->now();

    my $time = $now->sub( $self->{_start_time} );
    my $out  = <<"EOH";
Total run time: $time
EOH
    return $out;
}

# Given a generation number and a population, remember the population stats.
# The population is an arrayref of hashrefs containing a 'score' key.
sub add_population {
    my ( $self, $gen, $pop ) = @_;

    my @pop_by_score = sort { $a->{score} <=> $b->{score} } @$pop;
    my @scores       = map  { $_->{score} } @pop_by_score;

    my $mode = mode(@scores);
    my $max_mode;
    if ( $mode->is_multimodal() ) {
        $max_mode = $mode->query->query()->[-1];
    }
    else {
        $max_mode = $mode;
    }
    my @percentiles =
      map { $scores[ ( $_ / 100 ) * $#scores ] } 100, 95, 90, 75, 50,
      25;

    my $now = Time::HiRes::Value->now();
    my $time_taken;
    if (defined $self->{_last_gen}) {
        $time_taken = $now - $self->{_statistics}[$self->{_last_gen}]->{timestamp};
    } else {
        $time_taken = $now->sub( $self->{_start_time} );
    }

    # Always ensure Statistics::Basic results are converted to numbers,
    # or memory will leak.
    my %stats = (
        best        => $scores[-1],
        worst       => $scores[0],
        mean        => sprintf( '%.2f', mean(@scores) ),
        median      => sprintf( '%.0f', median(@scores) ),
        stddev      => sprintf( '%.2f', stddev(@scores) ),
        max_mode    => sprintf( '%.0f', $max_mode ),
        percentiles => \@percentiles,
        time        => sprintf( '%.2f', $time_taken ),
        timestamp   => "$now",
    );

    $self->{_statistics}[$gen] = \%stats;
    $self->{_last_gen} = $gen;

    if ( $self->recorder ) {
        $self->recorder->save( $gen, \%stats, [ $pop_by_score[-1] ] );
    }

    if ($self->renderer) {
        $self->renderer->animate($pop_by_score[-1]);
    }
}

sub get_latest_summary {
    my ($self) = @_;

    die "No statistics have been collected.\n"
      unless defined $self->{_last_gen};
    return $self->{_statistics}[ $self->{_last_gen} ];
}

sub get_latest_generation {
    my ($self) = @_;

    die "No statistics have been collected.\n"
      unless defined $self->{_last_gen};
    return $self->{_last_gen};
}

sub get_latest_text_summary {
    my ($self) = @_;

    my $summary = $self->get_latest_summary();
    my $gen     = $self->{_last_gen};

    # Rolling mean of the last 4 gen (generation numbers start at 1)
    my $earliest = $gen - 3 < 1 ? 1 : $gen - 3;
    my @times =
      map { $_->{time} } @{$self->{_statistics}}[ $earliest .. $gen ];
    my $time_per_gen = sprintf '%.2f', sum(@times) / @times;
    my $pc           = '[' . join( ',', $summary->{percentiles}->@* ) . ']';

    my $out = <<"EOH";
Generation $gen\tPercentiles: $pc
\tMedian: $summary->{median}\tMax Mode: $summary->{max_mode}\tWorst: $summary->{worst}\tTime per gen: $time_per_gen
EOH
    return $out;
}

__PACKAGE__->meta->make_immutable;
