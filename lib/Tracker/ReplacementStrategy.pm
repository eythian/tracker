package Tracker::ReplacementStrategy;

use Moose::Role;

# This role handles the transition from one generation to the next by
# creating new genotypes from old based on their scores (or really
# however it wants.)

# Given an arrayref of hashes which contain the keys 'score' and 'rep',
# it should return a new arrayref of hashes containing a 'rep' key.
requires 'make_new_generation';

1;
