package Tracker::ReplacementStrategy::Standard;

# Implements the replacement strategy as defined in the original paper.
#
# This takes the top n% and crossbreeds them at random to produce the new
# population.

use Moose;
use namespace::autoclean;

with 'Tracker::ReplacementStrategy';

has 'percent_to_take' => (
    isa      => 'Num',
    is       => 'ro',
    required => 1,
);

has 'genotype' => (
    does     => 'Tracker::Genotype',
    is       => 'ro',
    required => 1,
);

sub make_new_generation {
    my ( $self, $pop ) = @_;

    my @pop_by_score = sort { $b->{score} <=> $a->{score} } @$pop;
    my $to_take      = int( $self->percent_to_take / 100.0 * @pop_by_score );

    my @new_pop;
    $#new_pop = $#pop_by_score;

    my $genotype = $self->genotype;
    foreach my $i ( 0 .. @new_pop-1 ) {
        my $cand_1 = int( rand($to_take) );
        my $cand_2 = int( rand( $to_take - 1 ) );
        $cand_2++ if ( $cand_2 >= $cand_1 );  # prevent selecting the same twice
        my $new = $genotype->crossbreed( $pop_by_score[$cand_1]->{rep},
            $pop_by_score[$cand_2]->{rep} );
        $genotype->mutate($new);
        $new_pop[$i] = { rep => $new };
    }

    return \@new_pop;
}

__PACKAGE__->meta->make_immutable;
