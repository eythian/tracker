package Tracker::DFSA;

# This role takes a Deterministic Finite State Automata state machine and runs
# it.

use Moose;
use namespace::autoclean;

# The number of state transitions to terminate after
has 'max_run_states' => (
    isa     => 'Int',
    is      => 'ro',
    default => 200,
);

# The map object to run the DFSA on
has 'map' => (
    does     => 'Tracker::Map',
    is       => 'ro',
    required => 1,
);

# Runs the DFSA. Provide the initial state value and the state table to run.
#
# It will return the score based on the performance on the map.
#
# The state table is a nested array, the first index is the state number, the
# second is the input (provided by the map) in the forward direction. The
# values are a tuple where the first is:
# * 'N' = don't move
# * 'L' = turn left
# * 'R' = turn right
# * 'M' = move forward
# The second value is the new state number. Attempting to jump to a
# non-existent state is an error and will cause an exception when running.
# If C<$callback> is set, it'll be called on each state change (e.g.
# for animation.)
sub run {
    my ( $self, $initial_state, $state_table, $callback ) = @_;

    my $map = $self->map->get_map_clone();
    my ( $max_x, $max_y ) = $self->map->get_map_size();

    my $state = $initial_state;
    my $score = 0;
    my ( $x, $y ) = ( 0, 0 );
    my $dir = 'R';    # Up, Down, Left, Right, initially look right.

    if ($callback) {
        return if $callback->($x, $y, $dir);
    }
    for my $step ( 1 .. $self->max_run_states ) {
        my ( $fwd_x, $fwd_y ) = ( $x, $y );
        if ( $dir eq 'U' ) {
            $fwd_y--;
        }
        elsif ( $dir eq 'D' ) {
            $fwd_y++;
        }
        elsif ( $dir eq 'L' ) {
            $fwd_x--;
        }
        elsif ( $dir eq 'R' ) {
            $fwd_x++;
        }
        else {
            die "Invalid direction $dir in the state table.\n";
        }

        # We're on a toroid
        $fwd_x %= $max_x if ( $fwd_x >= $max_x || $fwd_x < 0);
        $fwd_y %= $max_y if ( $fwd_y >= $max_y || $fwd_y < 0);

        my $what_to_do = $state_table->[$state][ $map->[$fwd_x][$fwd_y] ];

        #  0 out the point.
        $map->[$x][$y] = 0;

        my $op = $what_to_do->[0];
        $state = $what_to_do->[1];

        # Op 'N' is do nothing
        if ( $op eq 'L' ) {
            $dir =
                $dir eq 'U' ? 'L'
              : $dir eq 'D' ? 'R'
              : $dir eq 'L' ? 'D'
              : $dir eq 'R' ? 'U'
              :   die "Invalid direction $dir in the state table.\n";
        }
        elsif ( $op eq 'R' ) {
            $dir =
                $dir eq 'U' ? 'R'
              : $dir eq 'D' ? 'L'
              : $dir eq 'L' ? 'U'
              : $dir eq 'R' ? 'D'
              :   die "Invalid direction $dir in the state table.\n";
        }
        elsif ( $op eq 'M' ) {
            $x = $fwd_x;
            $y = $fwd_y;

            # Maybe the score changed
            $score++ if ( $map->[$x][$y] );
        }
        if ($callback) {
            return if $callback->($x, $y, $dir);
        }
    }
    return { trail_squares => $score };
}

__PACKAGE__->meta->make_immutable;
