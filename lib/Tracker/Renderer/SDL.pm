package Tracker::Renderer::SDL;

# Animates the genotype using SDL

use Moose;
use namespace::autoclean;

use SDL;
use SDL::Event;
use SDL::Events;
use SDLx::App;
use SDLx::Sprite;
use Time::HiRes qw( sleep );

with 'Tracker::Renderer';

use constant {
    PX_SCALE => 20,
    WHITE    => 0xFFFFFFFF,
    BLACK    => 0x000000FF,
    GREY     => 0xDDDDDDFF,
    ARROW    => 0x33FF33FF,
    TRAIL    => 0xEB7575FF,
};

has 'map' => (
    does     => 'Tracker::Map',
    is       => 'ro',
    required => 1,
);

has 'genotype' => (
    does     => 'Tracker::Genotype',
    is       => 'ro',
    required => 1,
);

has 'dfsa' => (
    is       => 'Tracker::DFSA',
    is       => 'ro',
    required => 1,
);

has 'shutdown_callback' => (
    isa => 'CodeRef',
    is  => 'ro',
);

sub init {
    my ($self) = @_;

    local $SIG{INT};
    my ( $units_x, $units_y ) = $self->map->get_map_size();
    $self->{_units_x} = $units_x;
    $self->{_units_y} = $units_y;

    my ( $size_x, $size_y ) = (
        ( $units_x * PX_SCALE ),
        ( $units_y * PX_SCALE )
    );

    $self->{_size_x} = $size_x;
    $self->{_size_y} = $size_y;

    my $app = SDLx::App->new(
        title        => 'Tracker',
        width        => $size_x,
        height       => $size_y,
        depth        => 32,
        exit_on_quit => 1,
    );

    $self->{_sdl_app} = $app;
    my $map = $self->draw_map();
    $self->{_sdl_map} = $map;
    $map->blit($map);
    $app->update();
}

sub animate {
    my ($self, $rep) = @_;

    $self->do_events();

    my $app = $self->{_sdl_app};

    # Now take the genotype rep we have, and run the DFSA against it, but
    # calling us for each state change.
    my @geno = $self->genotype->get_state_table($rep->{rep});

    # Draw our sprite
    my $sprite_img = SDLx::Surface->new(
        height => PX_SCALE-1,
        width => PX_SCALE-1,
    );
    $sprite_img->draw_polygon_filled(
        [
            [ 0, PX_SCALE- 1 ],    # bottom-left
            [ int( ( PX_SCALE- 1 ) / 2 ), 0 ],    # top-middle point
            [ PX_SCALE- 1, PX_SCALE- 1 ],         # bottom-right point
            [ int( ( PX_SCALE- 1 ) / 2 ),
                int( ( PX_SCALE- 1 ) / 2 ) ]      # middle thingy
        ],
        ARROW,
        0                                         # antialiasing = true
    );
    my $sprite = SDLx::Sprite->new(
        surface => $sprite_img,
    );

    my $map = $self->{_sdl_map};
    my $trail = SDLx::Surface->new(
        width => $self->{_size_x},
        height => $self->{_size_y},
    );

    my $callback = sub {
        my ($x, $y, $dir) = @_;

        return 1 if ($self->do_events());

        my $draw_x = $x*PX_SCALE;
        my $draw_y = $y*PX_SCALE;
        $sprite->x($draw_x);
        $sprite->y($draw_y);
        my $rot = # anticlockwise
            $dir eq 'U' ? 0
          : $dir eq 'L' ? 90
          : $dir eq 'D' ? 180
          : $dir eq 'R' ? 275
          :               die "Unknown direction $dir";
        $sprite->rotation($rot);

        $map->blit($app);
        $trail->blit($app);
        $sprite->draw($app);
        $app->update();
        $trail->draw_rect(
            [ $x * PX_SCALE + 1, $y * PX_SCALE + 1, PX_SCALE- 1, PX_SCALE- 1 ],
            TRAIL
        );
        sleep(0.01);
        return 0;
    };

    $self->dfsa->run(@geno, $callback);
}

# Processes input events. Returns true if we should shutdown now.
sub do_events {
    my ($self) = @_;

    # First clear any pending events
    my $event = SDL::Event->new();
    SDL::Events::pump_events();
    while (SDL::Events::poll_event($event)) {
        if ($event->type == SDL_QUIT) {
            $self->shutdown_callback->();
            return 1; # signal a shutdown
        };
    }
}

# Renders the map onto a surface
sub draw_map {
    my ($self) = @_;

    my $size_x = $self->{_size_x};
    my $size_y = $self->{_size_y};
    my $units_x = $self->{_units_x};
    my $units_y = $self->{_units_y};
    my $surface = SDLx::Surface->new(
        width  => $size_x,
        height => $size_y,
    );

    $surface->draw_rect( [ 0, 0, $size_x, $size_y ], WHITE );
    for my $x ( 0 .. $units_x ) {
        $surface->draw_line( [ $x * PX_SCALE, 0 ],
            [ $x * PX_SCALE, $size_y ], BLACK );
    }
    for my $y ( 0 .. $units_y ) {
        $surface->draw_line( [ 0, $y * PX_SCALE ],
            [ $size_x, $y * PX_SCALE ], BLACK );
    }
    my $map = $self->map->get_human_map();
    for my $x ( 0 .. $units_x - 1 ) {
        for my $y ( 0 .. $units_y - 1 ) {
            my $val = $map->[$x][$y];
            if ( $val == 1 ) {    # black square
                $surface->draw_rect( [ $x * PX_SCALE+1, $y * PX_SCALE+1, PX_SCALE-1, PX_SCALE-1 ],
                    BLACK );
            }
            elsif ( $val == 2 ) {    # grey square
                $surface->draw_rect( [ $x * PX_SCALE+1, $y * PX_SCALE+1, PX_SCALE-1, PX_SCALE-1 ],
                    GREY );
            }
        }
    }
    return $surface;
}

__PACKAGE__->meta->make_immutable;
