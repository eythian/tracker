package Tracker::Evaluator;

# Implementations of this are able to compute the complete score for
# a given genotype representation.

use Moose::Role;
use namespace::autoclean;

# Computes the score given a representation
requires 'get_score';

1;
