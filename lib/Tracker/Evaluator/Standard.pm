package Tracker::Evaluator::Standard;

# This performs a standard scoring (i.e. how many trail squares an ant touches)
# as specified in the original paper.

use Moose;
use namespace::autoclean;

with 'Tracker::Evaluator';

# The state machine engine used.
has 'dfsa' => (
    does     => 'Tracker::DFSA',
    is       => 'ro',
    required => 1,
);

# The genotype object used
has 'genotype' => (
    does     => 'Tracker::Genotype',
    is       => 'ro',
    required => 1,
);

sub get_score {
    my ( $self, $rep ) = @_;

    my @state_machine = $self->genotype->get_state_table($rep);
    my $result = $self->dfsa->run(@state_machine);

    # We just care about the trail squares
    return $result->{trail_squares};
}

__PACKAGE__->meta->make_immutable;
