package Tracker::Renderer;

# Renders an animation of a given genotype on a map

use Moose::Role;
use namespace::autoclean;

# Called at the start
requires 'init';

# Provided a genotype, animates it.
requires 'animate';

1;
