package Tracker::Population;

use Moose;
use namespace::autoclean;

# This holds a set of representations and tracks their scores for an evaluation
# system to choose what to crossbreed etc.

# The genotype object to use
has 'genotype' => (
    does     => 'Tracker::Genotype',
    is       => 'ro',
    required => 1,
);

# The thing that computes the scores
has 'evaluator' => (
    does     => 'Tracker::Evaluator',
    is       => 'ro',
    required => 1,
);

# The population size
has 'size' => (
    isa      => 'Int',
    is       => 'ro',
    required => 1,
);

has 'replacement_strategy' => (
    does     => 'Tracker::ReplacementStrategy',
    is       => 'ro',
    required => 1,
);

has 'statistics' => (
    isa      => 'Tracker::Statistics',
    is       => 'ro',
    required => 1,
);

# Runs a generation step
sub step {
    my ($self) = @_;

    my $pop      = $self->{_population};
    my $size     = $self->size;
    my $genotype = $self->genotype;
    if ( exists $self->{_generation} ) {
        $self->{_generation}++;
    }
    else {
        $self->{_generation} = 1;
    }

    # Initialise the population if needed
    if ( !$pop || @$pop != $size ) {
        if ( $self->{_generation} != 1 ) {
            die
"Generation is $self->{_generation} but the population size is incorrect ("
              . scalar(@$pop)
              . "!=$size)\n";
        }
        my @new_pop;
        for ( 1 .. $size ) {
            my %new_rep = ( rep => $genotype->random, );
            push @new_pop, \%new_rep;
        }
        $self->{_population} = \@new_pop;
        $pop = \@new_pop;
    }

    # Evaluate all the scores that need it
    my $evaler = $self->evaluator;
    foreach my $rep (@$pop) {
        if ( !exists $rep->{score} ) {
            $rep->{score} = $evaler->get_score( $rep->{rep} );
        }
    }

    $self->statistics->add_population( $self->{_generation}, $pop );

    my $new_pop = $self->replacement_strategy->make_new_generation($pop);
    $self->{_population} = $new_pop;
}

__PACKAGE__->meta->make_immutable;
