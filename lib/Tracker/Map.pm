package Tracker::Map;

use Moose::Role;

# Retrieve a copy of the map, as an n×n array of booleans. This may be
# modified.
requires 'get_map_clone';

# Retrieves the map as an n×n array of 0 (blank), 1 (trail), 2 (virtual trail
# for rendering.) This might not be a copy, don't modify.
requires 'get_human_map';

# Retrieve a tuple containing the (x,y) dimensions of this map.
requires 'get_map_size';

# Get the maximum possible score for this map. May be undef if that can't be
# known.
requires 'get_max_score';

1;
