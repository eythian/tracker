package Tracker::Genotype::PlainBits;

use Moose;
use namespace::autoclean;

with 'Tracker::Genotype';

use Bit::Vector;

# This represents the genotypes as a sequence of bits, in the same fashion
# as the original paper.
#
# This uses Bit::Vector, it might be necessary to instead use C<vec> to
# avoid excessive object allocation. Testing needed.

use constant REP_LENGTH => 453;

# Mutation rate as a probability - in this case, the chance that any particular
# bit will be flipped when mutate is called.
has 'mutation_rate' => (
    isa     => 'Num',
    is      => 'rw',
);

# The rate at which the recombination operator switches between parents when
# producing the child.
has 'recombination_rate' => (
    isa => 'Num',
    is  => 'rw',
);

sub get_state_table {
    my ($self, $rep) = @_;

    my $start_vec = Bit::Vector->new(6);
    my $state_0 = Bit::Vector->new(6);
    my $state_1 = Bit::Vector->new(6);
    my $op_0 = Bit::Vector->new(2);
    my $op_1 = Bit::Vector->new(2);

    # Grab the first five bits, which is the starting state
    # If we're creating a number from this, leave the first 0 empty to force
    # it to be unsigned, by overallocating slightly (why it's inited with len 6.)
    $start_vec->Interval_Copy($rep, 0, 0, 5);
    my $start_state = $start_vec->to_Dec();

    my @state_table;

    # Each $i is an entry for a state.
    # 5 bits -> next state for input 0
    # 2 bits -> operation for input 0
    # 5 bits -> next state for input 1
    # 2 bits -> operation for input 1

    # We only produce the table for the reachable states
    my @reachable = ($start_state);
    while (defined(my $i = shift @reachable)) {
        next if defined $state_table[$i];
        my $offset = 5 + ( $i * 14 );

        $state_0->Interval_Copy( $rep, 0, $offset, 5 );
        $op_0->Interval_Copy( $rep, 0, $offset + 5, 2 );
        $state_1->Interval_Copy( $rep, 0, $offset + 5 + 2, 5 );
        $op_1->Interval_Copy( $rep, 0, $offset + 5 + 2 + 5, 2 );

        my $state_0_val = oct('0b' . $state_0->to_Bin());
        my $state_1_val = oct('0b' . $state_1->to_Bin());

        my $op_0_bin = $op_0->to_Bin();
        my $op_1_bin = $op_1->to_Bin();

        # Binary looks backwards due to Bit::Vector's output
        my $op_0_val =
            $op_0_bin eq '11' ? 'M'
          : $op_0_bin eq '10' ? 'L'
          : $op_0_bin eq '01' ? 'R'
          :                     'N';
        my $op_1_val =
            $op_1_bin eq '11' ? 'M'
          : $op_1_bin eq '10' ? 'L'
          : $op_1_bin eq '01' ? 'R'
          :                     'N';

        push @reachable, $state_0_val, $state_1_val;
        $state_table[$i] = [ [ $op_0_val, $state_0_val ], [ $op_1_val, $state_1_val ] ];
    }

    return ( $start_state, \@state_table );
}

sub random {
    my ($self) = @_;

    my $rep = Bit::Vector->new(REP_LENGTH);
    for my $i (0 .. REP_LENGTH-1) {
        if (int(rand(2))) {
            $rep->Bit_On($i);
        }
    }

    return $rep;
}

sub mutate {
    my ($self, $rep) = @_;

    my $rate = $self->mutation_rate();
    for my $i (0 .. REP_LENGTH-1) {
        if (rand(1) < $rate) {
            $rep->Interval_Flip($i,$i);
        }
    }
}

# Crossbreeds two representations and outputs one.
#
# The C<@test_random> is used for testing only, to replace the random numbers.
sub crossbreed {
    my ($self, $rep1, $rep2, @test_random) = @_;

    # first we'll determine our crossover points
    my $current_parent;
    my @crossovers;
    if (@test_random) {
        $current_parent = shift @test_random;
        @crossovers = @test_random;
    } else {
        my $rate = $self->recombination_rate;
        for my $i (0 .. REP_LENGTH-1) {
            if (rand(1) < $rate) {
                push @crossovers, $i;
            }
        }
        $current_parent = int(rand(2));
    }

    # Now allocate the new child and copy stuff into it.
    my $child = Bit::Vector->new(REP_LENGTH);
    my $parent_rep = $current_parent ? $rep2 : $rep1;
    push @crossovers, REP_LENGTH-1;
    my $prev_loc = 0;
    while ( defined( my $end_loc = shift @crossovers ) ) {
        my $len = $end_loc - $prev_loc;
        $child->Interval_Copy( $parent_rep, $prev_loc, $prev_loc, $len );
        $prev_loc       = $end_loc;
        $current_parent = !$current_parent;
        $parent_rep = $current_parent ? $rep2 : $rep1;
    }

    return $child;
}

sub serialise {
    my ($self, $rep) = @_;

    return $rep->to_Bin();
}

sub deserialise {
    my ($self, $rep) = @_;

    if (length $rep != REP_LENGTH) {
        die "Deserialised string length mismatch: maybe wrong genotype versions?";
    }
    return Bit::Vector->new_Bin(REP_LENGTH, $rep);
}

__PACKAGE__->meta->make_immutable;
