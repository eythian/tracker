package Tracker::Genotype::SimpleFSM;

use v5.10;

use Moose;
use namespace::autoclean;

with 'Tracker::Genotype';

# This represents and manipulates the FSM as a structure containing a
# list of states, rather than a binary representation.
# Essentially it's reproducing the original tracker system, but with a
# different encoding.
#
# It still has the order-list concept of states, so locality is a thing.

use constant {
    NUM_STATES => 32,
    NUM_OPS    => 4,
    OPS        => [ 'M', 'L', 'R', 'N' ],
    REV_OPS    => { M => 0, L => 1, R => 2, N => 3 },
};

# The mutation rate simulates the mutation of the binary representation.
# A state value is 5 bits and an action is 2 bits, this determines the chance
# of mutation, however the actual mutation may be to any new value (unlike
# the binary form, assuming a 1-bit change.)
has 'mutation_rate' => (
    isa     => 'Num',
    is      => 'rw',
    default => 0.001,
);

# Recombination rate also simulates the probabilities of the binary form,
# however the components of a state (for example, a new state value) will
# never be modified. It is possible for the crossover to split up components
# within a state though.
has 'recombination_rate' => (
    isa     => 'Num',
    is      => 'rw',
    default => 0.01,
);

# Create a random state machine. Internally a state machine is a big list:
# (init_state, state_0_op_0, state_0_newstate_0, state_0_op_1, state_0_newstate_1,
#  state_1_op_0 ... state_31_newstate_1)
sub random {
    my ($self) = @_;

    my @new_fsm;
    push @new_fsm, int( rand(NUM_STATES) );    # initial state
    foreach ( 0 .. NUM_STATES- 1 ) {
        push @new_fsm, OPS->[ int( rand(NUM_OPS) ) ],
          int( rand(NUM_STATES) );             # state part 0
        push @new_fsm, OPS->[ int( rand(NUM_OPS) ) ],
          int( rand(NUM_STATES) );             # state part 1
    }
    return \@new_fsm;
}

# Convert our long list representation into a state table
sub get_state_table {
    my ( $self, $rep ) = @_;

    my $start_state = $rep->[0];

    my @state_table;
    my @reachable = ($start_state);
    while ( defined( my $st = shift @reachable ) ) {
        next if defined $state_table[$st];
        my $offset = 1 + 4 * $st;
        $state_table[$st] = [
            [ $rep->[$offset],       $rep->[ $offset + 1 ] ],
            [ $rep->[ $offset + 2 ], $rep->[ $offset + 3 ] ]
        ];
        push @reachable, $rep->[ $offset + 1 ], $rep->[ $offset + 3 ];
    }
    return ( $start_state, \@state_table );
}

sub mutate {
    my ( $self, $rep ) = @_;

    # The rate assumes bits, so we pretend.
    my $rate = $self->mutation_rate();
    my @chance = ( 5 * $rate, 2 * $rate );
    my $is_op  = 0;
    foreach my $v (@$rep) {
        if ( rand(1) < $chance[$is_op] ) {
            my $new;
            if ($is_op) {
                my $v_num = REV_OPS->{$v};
                $new = int( rand( NUM_OPS - 1 ) );
                $new++ if $new >= $v_num;
                $new = OPS->[$new];
            }
            else {
                $new = int( rand( NUM_STATES - 1 ) );
                $new++ if $new >= $v;
            }
            $v = $new;
        }
        $is_op ^= 1;
    }
}

sub crossbreed {
    my ( $self, $rep1, $rep2, @test_random ) = @_;

    # determine crossover points
    my $current_parent;
    my @crossovers;
    if (@test_random) {
        ( $current_parent, @crossovers ) = @test_random;
    }
    else {
        $current_parent = int( rand(2) );
        my $rate = $self->mutation_rate();
        my @chance = ( 5 * $rate, 2 * $rate );
        my $is_op  = 0;
        for my $i ( scalar(@$rep1) - 1 ) {
            if ( rand(1) < $chance[$is_op] ) {
                push @crossovers, $i;
            }
            $is_op ^= 1;
        }
    }

    my @child;
    my $parent_rep = $current_parent ? $rep2 : $rep1;
    push @crossovers, scalar(@$rep1);
    my $prev_loc = 0;
    while ( defined( my $end_loc = shift @crossovers ) ) {
        push @child, @{$parent_rep}[ $prev_loc .. $end_loc - 1 ];
        $prev_loc       = $end_loc;
        $current_parent = !$current_parent;
        $parent_rep     = $current_parent ? $rep2 : $rep1;
    }
    return \@child;
}

sub serialise {
    my ( $self, $rep ) = @_;

    # we stringify just to make things smaller
    return join(',',@$rep);
}

sub deserialise {
    my ( $self, $rep ) = @_;

    return [ split(',',$rep) ];
}

__PACKAGE__->meta->make_immutable;
1;
